mod notifiers;
use serde::{Serialize, Deserialize};
use toml::Value;
use std::collections::HashMap;
use toml::toml;
use std::fs::File;
use failure::Error;
use std::io::prelude::*;

#[derive(Deserialize, Serialize, Debug)]
struct Config {
    services: Vec<Service>,
    websites: Vec<Website>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Service {
    name: String,
    telegram: Option<Telegram>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Website {
    name : String,
    url: String,
    notifier: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct Telegram {
    chatids: Vec<String>,
    apikey: String
}

fn main() -> Result<(), Error> {
    // Open config file
    let xdg_dirs = xdg::BaseDirectories::with_prefix("webwatchers").unwrap();
    let config_path = xdg_dirs.find_config_file("config.toml")
        .expect("cannot create configuration directory");
    let mut config_file = File::open(config_path)?;

    // Read and parse config file
    let mut file_content = String::new();
    config_file.read_to_string(&mut file_content)?;
    let config: Config = toml::from_str(&file_content)?;

    // send ping for each website
    for website in config.websites {
        // send ping to website
        let resp = reqwest::get(&website.url)?;
        if resp.status().is_server_error() {
            println!("Server error");
        } else if !resp.status().is_success() {
            println!("Error code {}", resp.status()); 
        }
    }

    Ok(())
}
