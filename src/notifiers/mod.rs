mod telegram;

extern crate telegram_bot;
use std::error::Error;

#[derive(Debug)]
pub struct notifier_error (String);

impl std::error::Error for notifier_error { }
impl std::fmt::Display for notifier_error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Notifier failed! Error is \"{}\"", self.0)
    }
}

impl std::convert::From<std::io::Error> for notifier_error {
    fn from(e:std::io::Error) -> Self {
        notifier_error(e.description().to_string())
    }
}
impl std::convert::From<telegram_bot::Error> for notifier_error {
    fn from(e:telegram_bot::Error) -> Self {
        notifier_error(e.description().to_string())
    }
}
pub trait Notify {
    fn send(&self, recipients : Vec<i64>, message: String) -> Result<(), notifier_error>;
}