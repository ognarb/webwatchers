extern crate futures;
extern crate telegram_bot;
extern crate tokio_core;

use std::str::FromStr;
use crate::notifiers::{notifier_error, Notify};
use telegram_bot::{SendMessage, ChatId};

struct telegram {
    core: tokio_core::reactor::Core,
    api: telegram_bot::Api,
}

impl telegram {
    fn new(api_key: String) -> Result<Self, notifier_error> {
        let mut core = tokio_core::reactor::Core::new()?;
        let api = telegram_bot::Api::configure(api_key).build(core.handle())?;
        Ok(telegram { core, api })
    }
}
impl Notify for telegram {
        fn send(&self, recipients : Vec<i64>, message: String) -> Result<(), notifier_error> {
            //TODO: only works when vec has length 1
            let mut core = tokio_core::reactor::Core::new().unwrap();
            let token = std::env::var("TELEGRAM_TOKEN").unwrap();
            println!("{}", token);
            let api = telegram_bot::Api::configure(token).build(core.handle()).unwrap();

            core.run(api.send(telegram_bot::SendMessage::new(telegram_bot::ChatId::new(recipients[0]) , "Hello World!")))?;
            Ok(())

        }
}
